import 'package:academic_master/domain/auth/user.dart';
import 'package:academic_master/domain/auth/value_objects.dart';
import 'package:academic_master/domain/core/value_objects.dart';

final mockUser = User(
  id: UniqueId(),
  name: Name("John Doe"),
  email: EmailAddress("amit@gmail.com"),
  contactNumber: PhoneNumber("9548582776"),
  college: College("Sample College"),
  course: Course("Sample Course"),
  branch: Branch("Sample Branch"),
  year: Year("2023"),
);

final emailAddress = EmailAddress("amitsingh@gmail.com");
final password = Password('Singh@6391');
