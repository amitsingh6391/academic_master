import 'package:academic_master/domain/auth/user.dart';
import 'package:academic_master/domain/auth/value_objects.dart';
import 'package:academic_master/domain/core/value_failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../mocks/mocks.dart';

void main() {
  group(
    'User',
    () {
      test(
        'should return a Failure when any value object is invalid',
        () {
          final result = mockUser
              .copyWith(email: EmailAddress('invalid-email'))
              .failureOption;
          expect(
            result,
            equals(
              some(
                const ValueFailure.invalidEmail('invalid-email'),
              ),
            ),
          );
        },
      );

      test(
        'should return none when no object is invalid',
        () {
          final result = mockUser.failureOption;
          expect(
            result,
            equals(none()),
          );
        },
      );
      test(
        'should return a Failure when any value object is empty',
        () {
          final user = User.empty();

          final result = user.failureOption;

          expect(result, equals(some(const ValueFailure.empty(''))));
        },
      );
    },
  );
}
