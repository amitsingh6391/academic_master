import 'package:academic_master/application/auth/sign_in_form/sign_in_form_bloc.dart';
import 'package:academic_master/domain/auth/i_auth_facade.dart';
import 'package:academic_master/domain/auth/value_objects.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import '../../mocks/mocks.dart';
import 'sign_in_form_bloc_test.mocks.dart';

@GenerateMocks([IAuthFacade])
void main() {
  final mockIAuthFacade = MockIAuthFacade();

  group(
    'signInForm bloc:',
    () {
      blocTest<SignInFormBloc, SignInFormState>(
        'should emit updated email state when email changes',
        build: () => SignInFormBloc(mockIAuthFacade),
        act: (bloc) {
          bloc.add(
            SignInFormEvent.emailChanged(mockUser.email.getorCrash()),
          );
        },
        expect: () => [
          SignInFormState.initial().copyWith(
            emailAddress: EmailAddress(
              mockUser.email.getorCrash(),
            ),
          ),
        ],
      );
      blocTest<SignInFormBloc, SignInFormState>(
        'should emit updated password state when password changes',
        build: () => SignInFormBloc(mockIAuthFacade),
        act: (bloc) {
          bloc.add(
            SignInFormEvent.passwordChanged(password.getorCrash()),
          );
        },
        expect: () => [
          SignInFormState.initial().copyWith(
            password: password,
          ),
        ],
      );
      blocTest<SignInFormBloc, SignInFormState>(
        'should emit sucess SignIn when email or password is valid',
        build: () {
          when(
            mockIAuthFacade.signInWithEmailAndPassword(
              emailAddress: emailAddress,
              password: password,
            ),
          ).thenAnswer(
            (realInvocation) async => const Right(unit),
          );
          return SignInFormBloc(mockIAuthFacade)
            ..emit(
              SignInFormState.initial(
                emailAddress: emailAddress,
                password: password,
              ),
            );
        },
        act: (bloc) => bloc.add(
          const SignInFormEvent.signInWithEmailAndPasswordPressed(),
        ),
        expect: () => [
          SignInFormState.initial().copyWith(
            isSubmitting: true,
            emailAddress: emailAddress,
            password: password,
            authFailureOrSuccessOption: none(),
          ),
          SignInFormState.initial().copyWith(
            isSubmitting: false,
            showErrorMessages: true,
            emailAddress: emailAddress,
            password: password,
            authFailureOrSuccessOption: const Some(Right(unit)),
          ),
        ],
      );
      blocTest<SignInFormBloc, SignInFormState>(
        'should emit errorMsg when email or password is invalid',
        build: () => SignInFormBloc(mockIAuthFacade),
        act: (bloc) {
          return bloc.add(
            const SignInFormEvent.signInWithEmailAndPasswordPressed(),
          );
        },
        expect: () => [
          SignInFormState.initial().copyWith(
            showErrorMessages: true,
          ),
        ],
      );
    },
  );
}
