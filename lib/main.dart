import 'package:academic_master/domain/core/push_notification_service.dart';
import 'package:academic_master/firebase_option.dart';
import 'package:academic_master/infrastructure/core/push_notification_service_impl.dart';
import 'package:academic_master/injection.dart';
import 'package:academic_master/presentation/core/app_widget.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';

import 'package:injectable/injectable.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: kIsWeb ? DefaultFirebaseOptions.currentPlatform : null,
  );
  configureInjection(Environment.prod);

  // call init of firebasePushNotification service
  final PushNotificationService pushNotificationService =
      PushNotificationServiceImpl();
  await pushNotificationService.initialize();

  runApp(
    AppWidget(), // Wrap your app
  );
}
