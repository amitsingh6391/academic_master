import 'package:academic_master/domain/core/value_failure.dart';
import 'package:academic_master/domain/core/value_objects.dart';
import 'package:academic_master/domain/e_learning/value_objects.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_comment.freezed.dart';

@freezed
abstract class UserComment implements _$UserComment {
  const factory UserComment({
    required UniqueId commentId,
    required UniqueId userId,
    required CommentDescription commentDescription,
    required Time commentAt,
  }) = _UserComment;
  const UserComment._();

  factory UserComment.empty() => UserComment(
        commentDescription: CommentDescription(" "),
        commentId: UniqueId(),
        userId: UniqueId(),
        commentAt: Time(""),
      );

  Option<ValueFailure<dynamic>> get failureOption {
    return commentDescription.failureOrUnit.fold(
      (f) => some(f),
      (r) => none(),
    );
  }
}
