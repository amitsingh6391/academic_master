import 'package:academic_master/domain/core/error/failure.dart';
import 'package:academic_master/domain/core/error/generic_failure.dart';
import 'package:academic_master/domain/core/error/network_failure.dart';
import 'package:academic_master/domain/core/network/response.dart';
import 'package:dartz/dartz.dart';

Future<Either<Failure, T>> tryExecuting<T>(
  Future<HttpResponse> Function() execute, {
  required T Function(HttpResponse) onSuccess,
}) async {
  try {
    final response = await execute();
    if (response.httpCode >= 200 && response.httpCode < 300) {
      return Right(onSuccess(response));
    }
    return Left(ServerFailure.fromResponse(response));
  } on SocketFailure catch (e) {
    return Left(e);
  } on TimeOutFailure catch (e) {
    return Left(e);
  } on ServerFailure catch (e) {
    return Left(e);
  } on Exception catch (e) {
    return Left(GenericFailure(message: e.toString()));
  }
}
