import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:academic_master/domain/core/error/network_failure.dart';
import 'package:academic_master/domain/core/network/network_client.dart';
import 'package:academic_master/domain/core/network/response.dart';
import 'package:http/http.dart' as http;
import 'package:http/retry.dart';

class AcademicMasterHttpClient implements NetworkClient {
  AcademicMasterHttpClient(
    http.Client client,
  ) : _retryClient = RetryClient(
          client,
          retries: 2,
          when: (response) {
            return response.statusCode == 401;
          },
        );

  String get _socketFailureText => 'Please check your Internet!';

  String get _timeOutFailureText => 'Try again later!';

  final RetryClient _retryClient;

  String _baseurl() =>
      "https://generativelanguage.googleapis.com/v1beta/models";

  @override
  Future<HttpResponse> get(
    String url, {
    Map<String, String>? headers,
    Map<String, dynamic>? queryParameters,
  }) async {
    try {
      final http.Response response = await _retryClient.get(
        Uri.parse(
          _baseurl() + url,
        ).replace(
          queryParameters: queryParameters,
        ),
        headers: headers,
      );

      return HttpResponse.fromHttpResponse(response);
    } on SocketException catch (_) {
      throw SocketFailure(message: _socketFailureText);
    } on TimeoutException catch (_) {
      throw TimeOutFailure(message: _timeOutFailureText);
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }

  @override
  Future<HttpResponse> post(
    String url, {
    Map<String, String>? headers,
    Map<String, dynamic>? body,
  }) async {
    try {
      final http.Response response = await _retryClient.post(
        Uri.parse(
          _baseurl() + url,
        ),
        body: jsonEncode(body),
        headers: headers,
      );
      return HttpResponse.fromHttpResponse(response);
    } on SocketException catch (_) {
      throw SocketFailure(message: _socketFailureText);
    } on TimeoutException catch (_) {
      throw TimeOutFailure(message: _timeOutFailureText);
    } on Exception catch (e) {
      throw Exception(e.toString());
    }
  }
}
