import 'package:academic_master/domain/core/network/response.dart';

abstract class NetworkClient {
  Future<HttpResponse> get(
    String url, {
    Map<String, String>? headers,
  });

  Future<HttpResponse> post(
    String url, {
    Map<String, String>? headers,
    Map<String, dynamic>? body,
  });
}
