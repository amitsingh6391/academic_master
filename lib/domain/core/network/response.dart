import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as http;

abstract class Response {
  int get httpCode;
}

class HttpResponse implements Response {
  const HttpResponse(
    this.httpCode, {
    required this.body,
    this.byteData,
    this.message,
  });

  @override
  final int httpCode;
  final Map<String, dynamic> body;
  final Uint8List? byteData;

  final String? message;

  Map<String, dynamic> get data => _parseDataFromBody();

  Map<String, dynamic> _parseDataFromBody() => body;

  factory HttpResponse.fromHttpResponse(http.Response response) {
    try {
      final String jsonResponseBody = response.body;

      if ((response.statusCode >= 200 && response.statusCode < 300) &&
          jsonDecode(jsonResponseBody) == null) {
        return HttpResponse(
          response.statusCode,
          body: {},
          message: '',
          byteData: response.bodyBytes,
        );
      } else {
        final Map<String, dynamic> body =
            jsonDecode(jsonResponseBody) as Map<String, dynamic>;
        return HttpResponse(
          response.statusCode,
          body: body,
          message: response.reasonPhrase,
          byteData: response.bodyBytes,
        );
      }
    } on Exception catch (_) {
      return HttpResponse(
        response.statusCode,
        body: {},
        message: response.body,
        byteData: response.bodyBytes,
      );
    }
  }
}

class MultipartResponse implements Response {
  @override
  final int httpCode;
  final Stream<double> stream;
  final Map<String, dynamic>? body;

  const MultipartResponse({
    required this.httpCode,
    required this.stream,
    this.body,
  });
}
