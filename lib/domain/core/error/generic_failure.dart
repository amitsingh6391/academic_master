import 'package:academic_master/domain/core/error/failure.dart';

class GenericFailure extends Failure implements Exception {
  const GenericFailure({super.message});

  @override
  bool? get stringify => false;
}
