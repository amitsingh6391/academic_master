import 'package:academic_master/domain/core/error/failure.dart';
import 'package:academic_master/domain/core/network/response.dart';

class NetworkFailure extends Failure implements Exception {
  const NetworkFailure({super.message});

  @override
  List<Object?> get props => [message];

  @override
  bool? get stringify => false;
}

class SocketFailure extends NetworkFailure {
  const SocketFailure({super.message});
}

class TimeOutFailure extends NetworkFailure {
  const TimeOutFailure({super.message});
}

class ServerFailure extends NetworkFailure {
  const ServerFailure({
    super.message,
    this.statusCode,
    this.code,
    this.requestId,
  });
  final int? statusCode;
  final int? code;
  final String? requestId;

  factory ServerFailure.fromResponse(Response response) {
    if (response is HttpResponse) {
      return ServerFailure(
        message: response.message,
        statusCode: response.httpCode,
      );
    } else {
      return ServerFailure(
        message: 'Unimplemented Error',
        statusCode: response.httpCode,
      );
    }
  }
}
