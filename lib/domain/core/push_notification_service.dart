/// Firebase for push notification.
/// Before using the [PushNotificationService] initiate the service
/// e.g.
/// ```
/// await Firebase.initializeApp();
/// ```
abstract class PushNotificationService {
  Future<void> initialize();
}
