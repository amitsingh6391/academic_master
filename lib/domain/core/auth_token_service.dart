abstract class TokenService {
  void initialize();
  void reinitialize();

  String? get currentUserID;
}
