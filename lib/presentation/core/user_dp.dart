import 'package:academic_master/application/e_learning/users_watcher/users_watcher_bloc.dart';
import 'package:academic_master/presentation/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class Userdp extends StatelessWidget {
  const Userdp({
    super.key,
    this.userName,
    this.circleAvatarColor,
    this.size,
  });
  final String? userName;
  final Color? circleAvatarColor;
  final double? size;
  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: circleAvatarColor ?? Apptheme.secondaryColor,
      radius: 25,
      child: BlocBuilder<UsersWatcherBloc, UsersWatcherState>(
        builder: (context, newState) {
          return newState.map(
            empty: (_) => const SizedBox(),
            initial: (value) => Text(
              userName != null ? userName!.substring(0, 1) : '',
              style: Apptheme(context).boldText.copyWith(
                    color: Apptheme.primaryColor,
                    fontSize: 20.sp,
                  ),
            ),
            loadFailure: (value) => const SizedBox(),
            loadInProgress: (value) => const SizedBox(),
            loadSuccess: (value) {
              return Text(
                (userName != null)
                    ? userName!.substring(0, 1)
                    : value.users.first.name.getorCrash().substring(0, 1),
                style: Apptheme(context).boldText.copyWith(
                      color: Apptheme.primaryColor,
                      fontSize: 20.sp,
                    ),
              );
            },
          );
        },
      ),
    );
  }
}
