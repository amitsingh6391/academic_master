import 'package:academic_master/application/auth/auth_bloc.dart';
import 'package:academic_master/presentation/routes/router.gr.dart';
import 'package:academic_master/presentation/theme/theme.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:line_icons/line_icon.dart';
import 'package:line_icons/line_icons.dart';

@RoutePage()
class HomePage extends StatelessWidget {
  const HomePage({
    this.intialIndex,
    super.key,
  });

  final int? intialIndex;

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(
        MediaQuery.of(context).size.width,
        MediaQuery.of(context).size.height,
      ),
    );

    return AutoTabsScaffold(
      routes: const [
        DashboardRoute(),
        SubjectsRoute(),
        ChatRoomRoute(),
        ProfileRoute(),
      ],
      bottomNavigationBuilder: (context, tabsRouter) => MultiBlocListener(
        listeners: [
          BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              state.maybeMap(
                unauthenticated: (_) => AutoRouter.of(context).replace(
                  const SignInRoute(),
                ),
                orElse: () {},
              );
            },
          ),
        ],
        child: BottomNavigationBar(
          currentIndex: tabsRouter.activeIndex,
          onTap: tabsRouter.setActiveIndex,
          items: _navBarsItems(),
        ),
      ),
    );
  }

  List<BottomNavigationBarItem> _navBarsItems() {
    return [
      _createTabMenu(icon: LineIcons.home),
      _createTabMenu(icon: LineIcons.bookReader),
      _createTabMenu(icon: LineIcons.facebookMessenger),
      _createTabMenu(icon: LineIcons.userGraduate),
    ];
  }

  BottomNavigationBarItem _createTabMenu({
    required IconData icon,
    String label = '',
  }) =>
      BottomNavigationBarItem(
        icon: LineIcon(
          icon,
          size: 35,
          color: CupertinoColors.systemGrey,
        ),
        activeIcon: LineIcon(
          icon,
          size: 35,
          color: Apptheme.primaryColor,
        ),
        label: label,
      );
}
