import 'package:academic_master/presentation/core/responsive.dart';
import 'package:academic_master/presentation/e_learning/e_learning_dashboard/question_and_comments/users_questions.dart';
import 'package:academic_master/presentation/e_learning/e_learning_dashboard/widgets/dashboard_appbar.dart';
import 'package:academic_master/presentation/theme/theme.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

@RoutePage()
class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      designSize: Size(
        MediaQuery.of(context).size.width,
        MediaQuery.of(context).size.height,
      ),
    );
    return Responsive(
      child: ColoredBox(
        color: Colors.white,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Apptheme.backgroundColor,
            body: Column(
              children: [
                Expanded(
                  child: DashBoardAppBar(),
                ),
                Expanded(
                  flex: 5,
                  child: SingleChildScrollView(
                    child: UsersQuestions(),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
