// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:academic_master/domain/e_learning/question.dart' as _i16;
import 'package:academic_master/presentation/auth/sign_in_page.dart' as _i8;
import 'package:academic_master/presentation/e_learning/chats_and_friends/chat_room_page.dart'
    as _i1;
import 'package:academic_master/presentation/e_learning/chats_and_friends/personal_chat_screen.dart'
    as _i5;
import 'package:academic_master/presentation/e_learning/chats_and_friends/students_group_chat_page.dart'
    as _i10;
import 'package:academic_master/presentation/e_learning/e_learning_dashboard/dashboard_page.dart'
    as _i2;
import 'package:academic_master/presentation/e_learning/e_learning_dashboard/question_and_comments/question_form.dart'
    as _i7;
import 'package:academic_master/presentation/e_learning/homepage.dart' as _i4;
import 'package:academic_master/presentation/e_learning/profile/edit_profile.dart'
    as _i3;
import 'package:academic_master/presentation/e_learning/profile/profile_page.dart'
    as _i6;
import 'package:academic_master/presentation/e_learning/subjects/subjects.dart'
    as _i11;
import 'package:academic_master/presentation/splash/splash_page.dart' as _i9;
import 'package:auto_route/auto_route.dart' as _i12;
import 'package:flutter/cupertino.dart' as _i13;
import 'package:flutter/foundation.dart' as _i15;
import 'package:flutter/material.dart' as _i14;

abstract class $AppRouter extends _i12.RootStackRouter {
  $AppRouter({super.navigatorKey});

  @override
  final Map<String, _i12.PageFactory> pagesMap = {
    ChatRoomRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.ChatRoomPage(),
      );
    },
    DashboardRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i2.DashboardPage(),
      );
    },
    EditProfileRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.EditProfilePage(),
      );
    },
    HomeRoute.name: (routeData) {
      final args =
          routeData.argsAs<HomeRouteArgs>(orElse: () => const HomeRouteArgs());
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i4.HomePage(
          intialIndex: args.intialIndex,
          key: args.key,
        ),
      );
    },
    PersonalChatScreen.name: (routeData) {
      final args = routeData.argsAs<PersonalChatScreenArgs>();
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i5.PersonalChatScreen(
          key: args.key,
          partnerId: args.partnerId,
        ),
      );
    },
    ProfileRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i6.ProfilePage(),
      );
    },
    QuestionFormRoute.name: (routeData) {
      final args = routeData.argsAs<QuestionFormRouteArgs>(
          orElse: () => const QuestionFormRouteArgs());
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i7.QuestionFormPage(
          key: args.key,
          editedQuestion: args.editedQuestion,
        ),
      );
    },
    SignInRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i8.SignInPage(),
      );
    },
    SplashRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i9.SplashPage(),
      );
    },
    StudentsGroupChatRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i10.StudentsGroupChatPage(),
      );
    },
    SubjectsRoute.name: (routeData) {
      return _i12.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: _i11.SubjectsPage(),
      );
    },
  };
}

/// generated route for
/// [_i1.ChatRoomPage]
class ChatRoomRoute extends _i12.PageRouteInfo<void> {
  const ChatRoomRoute({List<_i12.PageRouteInfo>? children})
      : super(
          ChatRoomRoute.name,
          initialChildren: children,
        );

  static const String name = 'ChatRoomRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i2.DashboardPage]
class DashboardRoute extends _i12.PageRouteInfo<void> {
  const DashboardRoute({List<_i12.PageRouteInfo>? children})
      : super(
          DashboardRoute.name,
          initialChildren: children,
        );

  static const String name = 'DashboardRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i3.EditProfilePage]
class EditProfileRoute extends _i12.PageRouteInfo<void> {
  const EditProfileRoute({List<_i12.PageRouteInfo>? children})
      : super(
          EditProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'EditProfileRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i4.HomePage]
class HomeRoute extends _i12.PageRouteInfo<HomeRouteArgs> {
  HomeRoute({
    int? intialIndex,
    _i13.Key? key,
    List<_i12.PageRouteInfo>? children,
  }) : super(
          HomeRoute.name,
          args: HomeRouteArgs(
            intialIndex: intialIndex,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const _i12.PageInfo<HomeRouteArgs> page =
      _i12.PageInfo<HomeRouteArgs>(name);
}

class HomeRouteArgs {
  const HomeRouteArgs({
    this.intialIndex,
    this.key,
  });

  final int? intialIndex;

  final _i13.Key? key;

  @override
  String toString() {
    return 'HomeRouteArgs{intialIndex: $intialIndex, key: $key}';
  }
}

/// generated route for
/// [_i5.PersonalChatScreen]
class PersonalChatScreen extends _i12.PageRouteInfo<PersonalChatScreenArgs> {
  PersonalChatScreen({
    _i14.Key? key,
    required String partnerId,
    List<_i12.PageRouteInfo>? children,
  }) : super(
          PersonalChatScreen.name,
          args: PersonalChatScreenArgs(
            key: key,
            partnerId: partnerId,
          ),
          initialChildren: children,
        );

  static const String name = 'PersonalChatScreen';

  static const _i12.PageInfo<PersonalChatScreenArgs> page =
      _i12.PageInfo<PersonalChatScreenArgs>(name);
}

class PersonalChatScreenArgs {
  const PersonalChatScreenArgs({
    this.key,
    required this.partnerId,
  });

  final _i14.Key? key;

  final String partnerId;

  @override
  String toString() {
    return 'PersonalChatScreenArgs{key: $key, partnerId: $partnerId}';
  }
}

/// generated route for
/// [_i6.ProfilePage]
class ProfileRoute extends _i12.PageRouteInfo<void> {
  const ProfileRoute({List<_i12.PageRouteInfo>? children})
      : super(
          ProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'ProfileRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i7.QuestionFormPage]
class QuestionFormRoute extends _i12.PageRouteInfo<QuestionFormRouteArgs> {
  QuestionFormRoute({
    _i15.Key? key,
    _i16.Question? editedQuestion,
    List<_i12.PageRouteInfo>? children,
  }) : super(
          QuestionFormRoute.name,
          args: QuestionFormRouteArgs(
            key: key,
            editedQuestion: editedQuestion,
          ),
          initialChildren: children,
        );

  static const String name = 'QuestionFormRoute';

  static const _i12.PageInfo<QuestionFormRouteArgs> page =
      _i12.PageInfo<QuestionFormRouteArgs>(name);
}

class QuestionFormRouteArgs {
  const QuestionFormRouteArgs({
    this.key,
    this.editedQuestion,
  });

  final _i15.Key? key;

  final _i16.Question? editedQuestion;

  @override
  String toString() {
    return 'QuestionFormRouteArgs{key: $key, editedQuestion: $editedQuestion}';
  }
}

/// generated route for
/// [_i8.SignInPage]
class SignInRoute extends _i12.PageRouteInfo<void> {
  const SignInRoute({List<_i12.PageRouteInfo>? children})
      : super(
          SignInRoute.name,
          initialChildren: children,
        );

  static const String name = 'SignInRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i9.SplashPage]
class SplashRoute extends _i12.PageRouteInfo<void> {
  const SplashRoute({List<_i12.PageRouteInfo>? children})
      : super(
          SplashRoute.name,
          initialChildren: children,
        );

  static const String name = 'SplashRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i10.StudentsGroupChatPage]
class StudentsGroupChatRoute extends _i12.PageRouteInfo<void> {
  const StudentsGroupChatRoute({List<_i12.PageRouteInfo>? children})
      : super(
          StudentsGroupChatRoute.name,
          initialChildren: children,
        );

  static const String name = 'StudentsGroupChatRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}

/// generated route for
/// [_i11.SubjectsPage]
class SubjectsRoute extends _i12.PageRouteInfo<void> {
  const SubjectsRoute({List<_i12.PageRouteInfo>? children})
      : super(
          SubjectsRoute.name,
          initialChildren: children,
        );

  static const String name = 'SubjectsRoute';

  static const _i12.PageInfo<void> page = _i12.PageInfo<void>(name);
}
