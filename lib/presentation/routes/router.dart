import 'package:academic_master/presentation/routes/router.gr.dart';
import 'package:auto_route/auto_route.dart';

@AutoRouterConfig(
  replaceInRouteName: 'Page,Route',
)
class AppRouter extends $AppRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();
  @override
  final List<AutoRoute> routes = [
    AutoRoute(
      page: SplashRoute.page,
      path: '/',
    ),
    AutoRoute(page: SignInRoute.page, path: '/sign-in-page'),
    AutoRoute(
      page: HomeRoute.page,
      children: [
        AutoRoute(page: DashboardRoute.page),
        AutoRoute(page: SubjectsRoute.page),
        AutoRoute(page: ChatRoomRoute.page),
        AutoRoute(page: ProfileRoute.page),
      ],
    ),
    AutoRoute(page: QuestionFormRoute.page),
    AutoRoute(page: StudentsGroupChatRoute.page),
    AutoRoute(page: PersonalChatScreen.page),
    AutoRoute(page: EditProfileRoute.page),
  ];
}
