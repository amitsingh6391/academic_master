part of 'sign_in_form_bloc.dart';

@freezed
class SignInFormState with _$SignInFormState {
  const factory SignInFormState({
    required EmailAddress emailAddress,
    required Password password,
    required bool showErrorMessages,
    required bool isSubmitting,
    required Option<Either<AuthFailure, Unit>> authFailureOrSuccessOption,
  }) = _SignInFormState;

  factory SignInFormState.initial({
    EmailAddress? emailAddress,
    Password? password,
    bool showErrorMessages = false,
    bool isSubmitting = false,
    Option<Either<AuthFailure, Unit>>? authFailureOrSuccessOption,
  }) =>
      SignInFormState(
        emailAddress: emailAddress ?? EmailAddress(""),
        password: password ?? Password(""),
        showErrorMessages: showErrorMessages,
        isSubmitting: isSubmitting,
        authFailureOrSuccessOption: authFailureOrSuccessOption ?? none(),
      );
}
