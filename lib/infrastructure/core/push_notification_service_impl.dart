import 'package:academic_master/domain/core/push_notification_service.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationServiceImpl implements PushNotificationService {
  @override
  Future<void> initialize() async {
    await FirebaseMessaging.instance.requestPermission(provisional: true);
  }
}
