import 'package:academic_master/domain/core/auth_token_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthTokenServiceImpl implements TokenService {
  String? _currentUserID;
  @override
  void initialize() {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      setUserID(user);
    }
  }

  @override
  String? get currentUserID => _currentUserID;
  void setUserID(User user) {
    _currentUserID = user.uid;
  }

  @override
  void reinitialize() {
    _currentUserID = null;
    initialize();
  }
}
