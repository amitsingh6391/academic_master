import 'package:academic_master/domain/core/auth_token_service.dart';
import 'package:academic_master/domain/core/push_notification_service.dart';
import 'package:academic_master/infrastructure/core/auth_token_service_impl.dart';
import 'package:academic_master/infrastructure/core/push_notification_service_impl.dart';
import "package:academic_master/injection.config.dart";
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

final GetIt getIt = GetIt.instance;

@injectableInit
void configureInjection(String env) {
  final AuthTokenServiceImpl authTokenServiceImpl = AuthTokenServiceImpl();
  $initGetIt(getIt, environment: env);

  authTokenServiceImpl.initialize();
  getIt
    ..registerSingleton<PushNotificationService>(
      PushNotificationServiceImpl(),
    )
    ..registerSingleton<TokenService>(
      authTokenServiceImpl,
    );
}
